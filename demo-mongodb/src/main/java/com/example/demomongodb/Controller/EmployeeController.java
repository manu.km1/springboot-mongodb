package com.example.demomongodb.Controller;

import com.example.demomongodb.Model.Employee;
import com.example.demomongodb.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @PostMapping("/employee")       // http://localhost:8080/employee
    public ResponseEntity<?> createEmployee(@RequestBody Employee employee){
        Employee employee1 = employeeRepository.save(employee);
        return new ResponseEntity<Employee>(employee1, HttpStatus.OK);
    }
    @GetMapping("/employee")       // http://localhost:8080/employee
    public ResponseEntity<?> getAllEmployees(){
        List<Employee> employees = employeeRepository.findAll();
        if(employees.size() > 0){
            return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
        }else{
            return new ResponseEntity<>("No Employee Available", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/employee/{id}")  // http://localhost:8080/employee/1
    public ResponseEntity<?> getSingleEmployee(@PathVariable("id") String id){
       Optional<Employee> optionalEmployee = employeeRepository.findById(id);
       if(optionalEmployee.isPresent()){
           return new ResponseEntity<>(optionalEmployee.get(), HttpStatus.OK);
       }else {
            return new ResponseEntity<>("Employee is not fount with this Id = " + id, HttpStatus.NOT_FOUND);
       }
    }

    @PutMapping("/employee/{id}")  // http://localhost:8080/employee/1
    public ResponseEntity<?> updateEmployee(@PathVariable("id") String id, @RequestBody Employee employee){
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if(optionalEmployee.isPresent()){
            Employee employeeToSave = optionalEmployee.get();
            employeeToSave.setFirstName(employee.getFirstName() != null ? employee.getFirstName() : employeeToSave.getFirstName());
            employeeToSave.setLastName(employee.getLastName() != null ? employee.getLastName() : employeeToSave.getLastName());
            employeeToSave.setEmail(employee.getEmail() != null ? employee.getEmail() : employeeToSave.getEmail());
            Employee employee1 = employeeRepository.save(employeeToSave);
            return new ResponseEntity<>(employee1, HttpStatus.OK);
        }else {
            return new ResponseEntity<>("Employee is not fount with this Id = " + id, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/employee/{id}") // http://localhost:8080/employee/1
    public ResponseEntity<?> deleteById(@PathVariable("id") String id){
        try {
            employeeRepository.deleteById(id);
            return new ResponseEntity<>("Successfully delete with ID = " + id, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
}